package commons;

public class Constants {

//	Customer Info
	public static final String NEW_CUS_CUSTOMER_ID_COL = "Customer ID";
	public static final String NEW_CUS_CUSTOMER_NAME_COL = "Customer Name";
	public static final String NEW_CUS_GENDER_COL = "Gender";
	public static final String NEW_CUS_BIRTHDATE_COL = "Birthdate";
	public static final String NEW_CUS_ADDRESS_COL = "Address";
	public static final String NEW_CUS_CITY_COL = "City";
	public static final String NEW_CUS_STATE_COL = "State";
	public static final String NEW_CUS_PIN_COL = "Pin";
	public static final String NEW_CUS_PHONE_COL = "Mobile No.";
	public static final String NEW_CUS_MAIL_COL = "Email";	
	
//	Account Info
	public static final String NEW_ACCOUNT_ID_COL = "Account ID";
	public static final String NEW_ACCOUNT_CUSTOMER_ID_COL = "Customer ID";
	public static final String NEW_ACCOUNT_CUSTOMER_NAME_COL = "Customer Name";
	public static final String NEW_ACCOUNT_MAIL_COL = "Email";
	public static final String NEW_ACCOUNT_TYPE_COL = "Account Type";
	public static final String NEW_ACCOUNT_DATE_OPEN_COL = "Date of Opening";
	public static final String NEW_ACCOUNT_CURRENT_AMOUNT_COL = "Current Amount";
	
//	Enquiry Info
	public static final String ENQUIRY_ACCOUNT_NO_COL = "Account No";
	public static final String ENQUIRY_ACCOUNT_TYPE_COL = "Type of Account";
	public static final String ENQUIRY_BALANCE_COL = "Balance";
	
//	Deposit Info
	public static final String DEPOSIT_TRANSACTION_ID_COL = "Transaction ID";
	public static final String DEPOSIT_ACC_NO_COL = "Account No";
	public static final String DEPOSIT_AMOUNT_CREDIT_COL = "Amount Credited";
	public static final String DEPOSIT_AMOUNT_DEBIT_COL = "Amount Debited";
	public static final String DEPOSIT_TRANSACTION_TYPE_COL = "Type of Transaction";
	public static final String DEPOSIT_DES_COL = "Description";
	public static final String DEPOSIT_CURRENT_BALANCE_COL = "Current Balance";
	
//	Fund transfer info
	public static final String TRANS_F_FROM_ACC_COL = "From Account Number";
	public static final String TRANS_F_TO_ACC_COL = "To Account Number";
	public static final String TRANS_F_AMOUNT_COL = "Amount";
	public static final String TRANS_F_DES_COL = "Description";
	
}
