package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.FundTransPageUI;

import commons.AbstractPage;
import commons.Constants;

public class FundTransPage extends AbstractPage{

	WebDriver driver;

	public FundTransPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void assertColumnValue(String column, String value) {
		String locate = String.format(FundTransPageUI.INFO_TEXT, column);
		assertTextEqual(driver, locate, value);
	}
	
	public String getFromAccount() {
		return getTextElement(driver, FundTransPageUI.INFO_TEXT, Constants.TRANS_F_FROM_ACC_COL);
	}
	
	public String getToAccount() {
		return getTextElement(driver, FundTransPageUI.INFO_TEXT, Constants.TRANS_F_TO_ACC_COL);
	}
	
	public String getAmount() {
		return getTextElement(driver, FundTransPageUI.INFO_TEXT, Constants.TRANS_F_AMOUNT_COL);
	}
	
	public String getDescription() {
		return getTextElement(driver, FundTransPageUI.INFO_TEXT, Constants.TRANS_F_DES_COL);
	}
	
}
