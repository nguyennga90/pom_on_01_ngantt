package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.EditCustomerPageUI;

import commons.AbstractPage;

public class EditCustomerPage extends AbstractPage {

	WebDriver driver;

	public EditCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputCustomerID(String customerID) {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TXT);
		sendKeyToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TXT, customerID);
	}
	
	public EditDetailCustomerPage clickSubmitBtn() {
		waitForControlVisible(driver, EditCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, EditCustomerPageUI.SUBMIT_BTN);
		return PageFactoryPage.getEditDetailCustomerPage(driver);
	}
}
