package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DepositInfoPageUI;

import commons.AbstractPage;
import commons.Constants;

public class DepositInfoPage extends AbstractPage{

	WebDriver driver;

	public DepositInfoPage(WebDriver driver) {
		this.driver = driver;
	}	

	public boolean isMsgDeposit(String accountNo) {
		String msg = String.format(DepositInfoPageUI.SUCCESS_TEXT, accountNo);
		return isControlIsDisplay(driver, msg);
	}
	
	public String getTransactionID() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_TRANSACTION_ID_COL);
	}
	
	public String getAccountNo() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_ACC_NO_COL);
	}
	
	public String getAmountCredited() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_AMOUNT_CREDIT_COL);
	}
	
	public String getTypeOfTransaction() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_TRANSACTION_TYPE_COL);
	}
	
	public String getDescription() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_DES_COL);
	}
	
	public String getCurrentBalance() {
		return getTextElement(driver, DepositInfoPageUI.INFO_TEXT, Constants.DEPOSIT_CURRENT_BALANCE_COL);
	}

}
