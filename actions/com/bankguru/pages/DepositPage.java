package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DepositPageUI;

import commons.AbstractPage;

public class DepositPage extends AbstractPage{

	WebDriver driver;

	public DepositPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void inputAccId(String accId) {
		waitForControlVisible(driver, DepositPageUI.ACCOUNT_NO_TXT);
		sendKeyToElement(driver, DepositPageUI.ACCOUNT_NO_TXT, accId);
	}
	
	public void inputAmount(String amount) {
		waitForControlVisible(driver, DepositPageUI.AMOUNT_TXT);
		sendKeyToElement(driver, DepositPageUI.AMOUNT_TXT, amount);
	}
	
	public void inputDescription(String des) {
		waitForControlVisible(driver, DepositPageUI.DES_TXT);
		sendKeyToElement(driver, DepositPageUI.DES_TXT, des);
	}
	
	public DepositInfoPage clickSubmitBtn() {
		waitForControlVisible(driver, DepositPageUI.SUBMIT_BTN);
		clickToElement(driver, DepositPageUI.SUBMIT_BTN);
		return PageFactoryPage.getDepositInfoPage(driver);
	}
}
