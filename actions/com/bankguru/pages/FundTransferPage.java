package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.FundTransInputPageUI;

import commons.AbstractPage;

public class FundTransferPage extends AbstractPage {

	WebDriver driver;

	public FundTransferPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputAccPayer(String accId) {
		waitForControlVisible(driver, FundTransInputPageUI.PAYER_ACC_NO_TXT);
		sendKeyToElement(driver, FundTransInputPageUI.PAYER_ACC_NO_TXT, accId);
	}

	public void inputAccPayee(String accId) {
		waitForControlVisible(driver, FundTransInputPageUI.PAYEE_ACC_NO_TXT);
		sendKeyToElement(driver, FundTransInputPageUI.PAYEE_ACC_NO_TXT, accId);
	}

	public void inputAmountTransfer(String amount) {
		waitForControlVisible(driver, FundTransInputPageUI.AMOUNT_TXT);
		sendKeyToElement(driver, FundTransInputPageUI.AMOUNT_TXT, amount);
	}

	public void inputDescription(String des) {
		waitForControlVisible(driver, FundTransInputPageUI.DESCRIPTION_TXT);
		sendKeyToElement(driver, FundTransInputPageUI.DESCRIPTION_TXT, des);
	}

	public FundTransPage clickSubmitBtn() {
		waitForControlVisible(driver, FundTransInputPageUI.SUBMIT_BTN);
		clickToElement(driver, FundTransInputPageUI.SUBMIT_BTN);
		return PageFactoryPage.getFundTransPage(driver);
	}
	
	public FundTransPage transferToOtherAcc(String payer, String payee,
			String amount, String des) {
		// Fill data
		inputAccPayer(payer);
		inputAccPayee(payee);
		inputAmountTransfer(amount);
		inputDescription(des);

		// Click Submit button
		return clickSubmitBtn();
	}

}
