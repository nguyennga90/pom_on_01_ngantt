package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.CustomerUpdateMsgPageUI;

import commons.AbstractPage;
import commons.Constants;

public class CustomerUpdateMsgPage extends AbstractPage{

	WebDriver driver;

	public CustomerUpdateMsgPage(WebDriver driver) {
		this.driver = driver;
	}	

	public boolean isUpdateCustomerSuccess() {
		return isControlIsDisplay(driver, CustomerUpdateMsgPageUI.SUCCESS_TEXT);
	}
	
	public String getCustomerID() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_CUSTOMER_ID_COL);
	}
	
	public String getCustomerName() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_CUSTOMER_NAME_COL);
	}
	
	public String getGender() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_GENDER_COL);
	}
	
	public String getBirthdate() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_BIRTHDATE_COL);
	}
	
	public String getAddress() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_ADDRESS_COL);
	}
	
	public String getCity() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_CITY_COL);
	}
	
	public String getState() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_STATE_COL);
	}
	
	public String getPin() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_PIN_COL);
	}
	
	public String getPhoneNo() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_PHONE_COL);
	}
	
	public String getEmail() {
		return getTextElement(driver, CustomerUpdateMsgPageUI.INFO_TEXT, Constants.NEW_CUS_MAIL_COL);
	}
}
