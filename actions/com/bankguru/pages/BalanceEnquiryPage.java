package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.BalanceEnquiryPageUI;

import commons.AbstractPage;

public class BalanceEnquiryPage extends AbstractPage{

	WebDriver driver;

	public BalanceEnquiryPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputAccNo(String acc) {
		waitForControlVisible(driver, BalanceEnquiryPageUI.ACC_NO_TXT);
		sendKeyToElement(driver, BalanceEnquiryPageUI.ACC_NO_TXT, acc);
	}
	
	public BalEnquiryPage clickSubmitBtn() {
		waitForControlVisible(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return PageFactoryPage.getBalEnquiryPage(driver);
	}
}
