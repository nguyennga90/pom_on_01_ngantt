package com.bankguru.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.bankguru.ui.NewCustomerPageUI;

import commons.AbstractPage;

public class NewCustomerPage extends AbstractPage {
	WebDriver driver;

	public NewCustomerPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void inputCustomerName(String customerName) {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT, customerName);
	}

	public void inputCustomerName(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT, key);
	}

	public void chooseGenderMale() {
		waitForControlVisible(driver, NewCustomerPageUI.GENDER_MALE_RDO);
		clickToElement(driver, NewCustomerPageUI.GENDER_MALE_RDO);
	}

	public void chooseGenderFemale() {
		waitForControlVisible(driver, NewCustomerPageUI.GENDER_FEMALE_RDO);
		clickToElement(driver, NewCustomerPageUI.GENDER_FEMALE_RDO);
	}

	public void inputBirthday(String birthday) {
		waitForControlVisible(driver, NewCustomerPageUI.DATE_OF_BIRTH);
		sendKeyToElement(driver, NewCustomerPageUI.DATE_OF_BIRTH, birthday);
	}
	
	public void inputAddress(String address) {
		waitForControlVisible(driver, NewCustomerPageUI.ADDRESS_TXA);
		sendKeyToElement(driver, NewCustomerPageUI.ADDRESS_TXA, address);
	}

	public void inputAddress(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.ADDRESS_TXA);
		sendKeyToElement(driver, NewCustomerPageUI.ADDRESS_TXA, key);
	}
	
	public void inputCity(String city) {
		waitForControlVisible(driver, NewCustomerPageUI.CITY_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.CITY_TXT, city);
	}
	
	public void inputCity(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.CITY_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.CITY_TXT, key);
	}
	
	public void inputState(String state) {
		waitForControlVisible(driver, NewCustomerPageUI.STATE_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.STATE_TXT, state);
	}

	public void inputState(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.STATE_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.STATE_TXT, key);
	}
	
	public void inputPin(String pin) {
		waitForControlVisible(driver, NewCustomerPageUI.PIN_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.PIN_TXT, pin);
	}

	public void inputPin(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.PIN_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.PIN_TXT, key);
	}
	
	public void inputPhoneNo(String phoneNo) {
		waitForControlVisible(driver, NewCustomerPageUI.PHONE_NO_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.PHONE_NO_TXT, phoneNo);
	}

	public void inputPhoneNo(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.PHONE_NO_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.PHONE_NO_TXT, key);
	}
	
	public void inputEmail(String mail) {
		waitForControlVisible(driver, NewCustomerPageUI.EMAIL_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.EMAIL_TXT, mail);
	}

	public void inputEmail(Keys key) {
		waitForControlVisible(driver, NewCustomerPageUI.EMAIL_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.EMAIL_TXT, key);
	}

	public void inputPass(String pass) {
		waitForControlVisible(driver, NewCustomerPageUI.PASSWORD_TXT);
		sendKeyToElement(driver, NewCustomerPageUI.PASSWORD_TXT, pass);
	}

	public CustomerRegMsgPage clickSubmitBtn() {
		waitForControlVisible(driver, NewCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, NewCustomerPageUI.SUBMIT_BTN);
		return PageFactoryPage.getCustomerRegMsgPage(driver);
	}
	
	public boolean isDisplayEmptyNameMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.CUSTOMER_NAME_EMPTY_TEXT);
	}
	
	public boolean isDisplayNumericMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.CUSTOMER_NAME_NUMERIC_TEXT);
	}
	
	public boolean isDisplaySpecialMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.CUSTOMER_NAME_SPECIAL_TEXT);
	}
	
	public boolean isDisplayBlankCharacterMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.CUSTOMER_NAME_CHARACTER_TEXT);
	}
	
	public boolean isDisplayEmptyAddressMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.ADDRESS_EMPTY_TEXT);
	}
	
	public boolean isDisplayEmptyCityMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.CITY_EMPTY_TEXT);
	}
	
	public boolean isDisplayEmptyStateMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.STATE_EMPTY_TEXT);
	}
	
	public boolean isDisplayEmptyPinMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.PIN_EMPTY_TEXT);
	}
	
	public boolean isDisplayPinLessCharacterMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.PIN_LESS_CHARACTER_TEXT);
	}
	
	public boolean isDisplayEmptyPhoneMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.PHONE_EMPTY_TEXT);
	}
	
	public boolean isDisplayEmptyMailMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.MAIL_EMPTY_TEXT);
	}
	
	public boolean isDisplayIllegalCharacterMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.ILLEGAL_CHARACTER_TEXT);
	}

	public boolean isDisplayInvalidMailMsg() {
		return isControlIsDisplay(driver, NewCustomerPageUI.INVALID_EMAIL_TEXT);
	}
}
