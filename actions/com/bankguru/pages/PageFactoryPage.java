package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

public class PageFactoryPage {

	public static LoginPage getLoginPage(WebDriver driver) {
		return new LoginPage(driver);
	}

	public static RegisterPage getRegisterPage(WebDriver driver) {
		return new RegisterPage(driver);
	}

	public static HomePage getHomePage(WebDriver driver) {
		return new HomePage(driver);
	}
	
	public static NewCustomerPage getNewCustomerPage(WebDriver driver) {
		return new NewCustomerPage(driver);
	}
	
	public static EditCustomerPage getEditCustomerPage(WebDriver driver) {
		return new EditCustomerPage(driver);
	}
	
	public static DeleteCustomerPage getDeleteCustomerPage(WebDriver driver) {
		return new DeleteCustomerPage(driver);
	}
	
	public static NewAccountPage getNewAccountPage(WebDriver driver) {
		return new NewAccountPage(driver);
	}
	
	public static EditAccountPage getEditAccountPage(WebDriver driver) {
		return new EditAccountPage(driver);
	}
	
	public static DeleteAccountPage getDeleteAccountPage(WebDriver driver) {
		return new DeleteAccountPage(driver);
	}
	
	public static DepositPage getDepositPage(WebDriver driver) {
		return new DepositPage(driver);
	}
	
	public static WithdrawalPage getWithdrawalPage(WebDriver driver) {
		return new WithdrawalPage(driver);
	}
	
	public static FundTransferPage getFundTransferPage(WebDriver driver) {
		return new FundTransferPage(driver);
	}
	
	public static ChangePasswordPage getChangePasswordPage(WebDriver driver) {
		return new ChangePasswordPage(driver);
	}
	
	public static BalanceEnquiryPage getBalanceEnquiryPage(WebDriver driver) {
		return new BalanceEnquiryPage(driver);
	}
	
	public static MiniStatementPage getMiniStatementPage(WebDriver driver) {
		return new MiniStatementPage(driver);
	}
	
	public static CustomisedStatementPage getCustomisedStatementPage(WebDriver driver) {
		return new CustomisedStatementPage(driver);
	}

	public static LogOutPage getLogOutPage(WebDriver driver) {
		return new LogOutPage(driver);
	}

	public static CustomerRegMsgPage getCustomerRegMsgPage(WebDriver driver) {
		return new CustomerRegMsgPage(driver);
	}

	public static EditDetailCustomerPage getEditDetailCustomerPage(WebDriver driver) {
		return new EditDetailCustomerPage(driver);
	}

	public static CustomerUpdateMsgPage getCustomerUpdateMsgPage(WebDriver driver) {
		return new CustomerUpdateMsgPage(driver);
	}

	public static AccCreateMsgPage getAccCreateMsgPage(WebDriver driver) {
		return new AccCreateMsgPage(driver);
	}

	public static DepositInfoPage getDepositInfoPage(WebDriver driver) {
		return new DepositInfoPage(driver);
	}

	public static WithdrawalInfoPage getWithdrawalInfoPage(WebDriver driver) {
		return new WithdrawalInfoPage(driver);
	}

	public static FundTransPage getFundTransPage(WebDriver driver) {
		return new FundTransPage(driver);
	}

	public static BalEnquiryPage getBalEnquiryPage(WebDriver driver) {
		return new BalEnquiryPage(driver);
	}
}
