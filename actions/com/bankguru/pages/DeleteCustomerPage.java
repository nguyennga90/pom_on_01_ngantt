package com.bankguru.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DeleteCustomerPageUI;

import commons.AbstractPage;

public class DeleteCustomerPage extends AbstractPage{

	WebDriver driver;
	Alert alert;

	public DeleteCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputCutomerId(String customerId) {
		waitForControlVisible(driver, DeleteCustomerPageUI.CUSTOMER_ID_TXT);
		sendKeyToElement(driver, DeleteCustomerPageUI.CUSTOMER_ID_TXT, customerId);
	}
	
	public HomePage clickSubmitBtn() {
		waitForControlVisible(driver, DeleteCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, DeleteCustomerPageUI.SUBMIT_BTN);
		alert = switchToAlert(driver);
		acceptAlert(alert);
		waitForControlAlertPresence(driver);
		if (!driver.toString().toLowerCase().contains("internetexplorer")) {
			String msgDelete = getTextAlert(alert);
			assertTextEqual(DeleteCustomerPageUI.MSG_SUCCESS, msgDelete);
		}
		acceptAlert(alert);
		return PageFactoryPage.getHomePage(driver);
	}

	public HomePage deleteCustomer(String customerId) {
		// Input AccountID
		inputCutomerId(customerId);

		// Click Submit button
		return clickSubmitBtn();
	}

}
