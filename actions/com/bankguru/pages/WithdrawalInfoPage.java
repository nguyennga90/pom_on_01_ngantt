package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.WithdrawalInfoPageUI;

import commons.AbstractPage;
import commons.Constants;

public class WithdrawalInfoPage extends AbstractPage{

	WebDriver driver;

	public WithdrawalInfoPage(WebDriver driver) {
		this.driver = driver;
	}	

	public boolean isMsgWithdrawal(String accountNo) {
		String msg = String.format(WithdrawalInfoPageUI.SUCCESS_TEXT, accountNo);
		return isControlIsDisplay(driver, msg);
	}
	
	public String getTransactionID() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_TRANSACTION_ID_COL);
	}
	
	public String getAccountNo() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_ACC_NO_COL);
	}
	
	public String getAmountDebited() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_AMOUNT_DEBIT_COL);
	}
	
	public String getTypeOfTransaction() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_TRANSACTION_TYPE_COL);
	}
	
	public String getDescription() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_DES_COL);
	}
	
	public String getCurrentBalance() {
		return getTextElement(driver, WithdrawalInfoPageUI.INFO_TEXT, Constants.DEPOSIT_CURRENT_BALANCE_COL);
	}

}
