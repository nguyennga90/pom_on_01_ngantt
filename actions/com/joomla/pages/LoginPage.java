package com.joomla.pages;

import org.openqa.selenium.WebDriver;

import com.joomla.ui.LoginPageUI;

import commons.AbstractPage;

public class LoginPage extends AbstractPage{

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	

	public void setUserName(String userId) {
		waitForControlVisible(driver, LoginPageUI.USER_TXT);
		sendKeyToElement(driver, LoginPageUI.USER_TXT, userId);
	}

	public void setPassword(String password) {
		waitForControlVisible(driver, LoginPageUI.PASS_TXT);
		sendKeyToElement(driver, LoginPageUI.PASS_TXT, password);

	}

	public IndexPage clickLoginBtn() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		if (driver.toString().toLowerCase().contains("ie")) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return PageFactoryPage.getIndexPage(driver);
	}
}
