package com.joomla.pages;

import org.openqa.selenium.WebDriver;

public class PageFactoryPage {
	
	public static LoginPage getLoginPage(WebDriver driver) {
		return new LoginPage(driver);
	}
	
	public static IndexPage getIndexPage(WebDriver driver) {
		return new IndexPage(driver);
	}

	public static NewArticlePage getNewArticlePage(WebDriver driver) {
		return new NewArticlePage(driver);
	}

	public static ArticlePage getArticlePage(WebDriver driver) {
		// TODO Auto-generated method stub
		return new ArticlePage(driver);
	}
}
