package com.joomla.pages;

import org.openqa.selenium.WebDriver;
import com.joomla.ui.NewArticlePageUI;

import commons.AbstractPage;

public class NewArticlePage extends AbstractPage{

	WebDriver driver;

	public NewArticlePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public ArticlePage createNewArticle(String title, String category, String content) {
		waitForControlVisible(driver, NewArticlePageUI.TITLE_TXT);
		sendKeyToElement(driver, NewArticlePageUI.TITLE_TXT, title);
		
		waitForControlVisible(driver, NewArticlePageUI.CATEGORY_CBX);
		clickToElement(driver, NewArticlePageUI.CATEGORY_CBX);
		
		waitForControlVisible(driver, NewArticlePageUI.CATEGORY_ITEM, category);
		clickToElement(driver, NewArticlePageUI.CATEGORY_ITEM, category);
		
		switchToIframe(driver, NewArticlePageUI.ARTICLE_IFRAME);
		setTextToElement(driver, "p", content);
		
		backDefaultWindow(driver);
		clickToElement(driver, NewArticlePageUI.SAVE_CLOSE_BTN);
		return PageFactoryPage.getArticlePage(driver);
	}
}
