package com.joomla.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.joomla.ui.IndexPageUI;

import commons.AbstractPage;

public class IndexPage extends AbstractPage{

	WebDriver driver;
	Actions action;

	public IndexPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public NewArticlePage goToNewAriclePage() {
		action = setAction(driver);
		
		waitForControlVisible(driver, IndexPageUI.CONTENT_MENU);
		clickToElement(driver, IndexPageUI.CONTENT_MENU);
		waitForControlVisible(driver, IndexPageUI.ARTICLE_MENU);
		hoverMouse(driver, IndexPageUI.ARTICLE_MENU, action);
		waitForControlVisible(driver, IndexPageUI.NEW_ARTICLE_MENU);
		singleClick(driver, IndexPageUI.NEW_ARTICLE_MENU, action);
		return PageFactoryPage.getNewArticlePage(driver);
	}
	
	public ArticlePage goToAriclePage() {
		waitForControlVisible(driver, IndexPageUI.CONTENT_MENU);
		clickToElement(driver, IndexPageUI.CONTENT_MENU);
		waitForControlVisible(driver, IndexPageUI.ARTICLE_MENU);
		clickToElement(driver, IndexPageUI.ARTICLE_MENU);
		return PageFactoryPage.getArticlePage(driver);
	}
}
