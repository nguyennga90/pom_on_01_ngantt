package com.joomla.pages;

import org.openqa.selenium.WebDriver;

import com.joomla.ui.ArticlePageUI;
import com.joomla.ui.IndexPageUI;

import commons.AbstractPage;

public class ArticlePage extends AbstractPage{

	WebDriver driver;

	public ArticlePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean isDisplayArticle(String title) {
		return isControlIsDisplay(driver, ArticlePageUI.ARTICLE_TITLE_LNK, title);
	}

	public ArticlePage goToAriclePage() {
		waitForControlVisible(driver, IndexPageUI.CONTENT_MENU);
		clickToElement(driver, IndexPageUI.CONTENT_MENU);
		waitForControlVisible(driver, IndexPageUI.ARTICLE_MENU);
		clickToElement(driver, IndexPageUI.ARTICLE_MENU);
		return PageFactoryPage.getArticlePage(driver);
	}
	
	public NewArticlePage editArticle(String oldTitle){
		waitForControlVisible(driver, ArticlePageUI.ARTICLE_CBX, oldTitle);
		clickToElement(driver, ArticlePageUI.ARTICLE_CBX, oldTitle);
		
		waitForControlVisible(driver, ArticlePageUI.EDIT_BTN);
		clickToElement(driver, ArticlePageUI.EDIT_BTN);
		return PageFactoryPage.getNewArticlePage(driver);
	}
	
}
