package com.bankguru.register;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.bankguru.pages.HomePage;
import com.bankguru.pages.LoginPage;
import com.bankguru.pages.NewCustomerPage;
import com.bankguru.pages.PageFactoryPage;
import com.bankguru.pages.RegisterPage;

import commons.AbstractTest;

public class Register_01_ValidateNewCustomer extends AbstractTest {
	WebDriver driver;

	String mail, loginURL, userId, password, strNumericName1, strNumericName2, strSpecialName1, strSpecialName2,
			strBlankName, strBlankAddress, strBlankCity, strNumericCity1, strNumericCity2, strSpecialCity1,
			strSpecialCity2, strNumericState1, strNumericState2, strSpecialState1, strSpecialState2, strBlankState,
			strBlankPin, strNumericPin, strSpecialPin1, strSpecialPin2, strBlank, strLessPin, strBlankPhone1,
			strBlankPhone2, strSpecialPhone1, strSpecialPhone2, strSpecialPhone3, strBlankEmail, strInvalidEmail1,
			strInvalidEmail2, strInvalidEmail3, strInvalidEmail4, strInvalidEmail5;

	private LoginPage loginPage;
	private RegisterPage registerPage;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;

	@Parameters({ "browser", "url", "version" })
	@BeforeClass
	public void beforeClass(String browser, String url, @Optional("firefox") String version) {
		driver = openMultiBrowser(browser, url, version);
		mail = "ngantt" + randomNumber() + "@gmail.com";

		strNumericName1 = "1234";
		strNumericName2 = "name123";
		strSpecialName1 = "name!@#";
		strSpecialName2 = "!@#";
		strBlankName = " name";

		strBlankAddress = " address";

		strBlankCity = " city";
		strNumericCity1 = "1234";
		strNumericCity2 = "city123";
		strSpecialCity1 = "city!@#";
		strSpecialCity2 = "!@#";

		strBlankState = " State";
		strNumericState1 = "1234";
		strNumericState2 = "State123";
		strSpecialState1 = "State!@#";
		strSpecialState2 = "!@#";

		strBlankPin = " Pin";
		strNumericPin = "Pin123";
		strSpecialPin1 = "Pin!@#";
		strSpecialPin2 = "!@#";
		strLessPin = "12";

		strBlankPhone1 = " 123";
		strBlankPhone2 = "12 12";
		strSpecialPhone1 = "886636!@12";
		strSpecialPhone2 = "!@88662682";
		strSpecialPhone3 = "88663682!@";

		strBlankEmail = " 12@gmail.com";
		strInvalidEmail1 = "guru99@gmail";
		strInvalidEmail2 = "guru99";
		strInvalidEmail3 = "guru99@";
		strInvalidEmail4 = "guru99@gmail.";
		strInvalidEmail5 = "guru99gmail.com";
	}

	@Test
	public void Register_00_CreateGuruAccount() {

		// Create account
		loginPage = PageFactoryPage.getLoginPage(driver);
		loginURL = loginPage.getCurrentUrl();
		registerPage = loginPage.clickHereLink();

		registerPage.regisNewMail(mail);
		registerPage.clickSubmitBtn();
		userId = registerPage.getUserInfo();
		password = registerPage.getPasswordInfo();

		// Login
		loginPage = registerPage.returnLoginPage(loginURL);

		loginPage.enterUserId(userId);
		loginPage.enterPassword(password);
		homePage = loginPage.clickSubmitBtn();
	}

	@Test
	public void Register_01_NameEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in NAME Field
		newCustomerPage.inputCustomerName(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyNameMsg());
	}

	@Test
	public void Register_02_1_NumericName() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in NAME Field
		newCustomerPage.inputCustomerName(strNumericName1);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_02_2_NumericName() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in NAME Field
		newCustomerPage.inputCustomerName(strNumericName2);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_03_1_SpecialName() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In NAME Field
		newCustomerPage.inputCustomerName(strSpecialName1);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_03_2_SpecialName() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In NAME Field
		newCustomerPage.inputCustomerName(strSpecialName2);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_04_BlankName() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputCustomerName(strBlankName);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_05_AddressEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in Address Field
		newCustomerPage.inputAddress(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyAddressMsg());
	}

	@Test
	public void Register_06_BlankAddress() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputAddress(strBlankAddress);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_07_CityEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in city Field
		newCustomerPage.inputCity(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyCityMsg());
	}

	@Test
	public void Register_08_1_NumericCity() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in City Field
		newCustomerPage.inputCity(strNumericCity1);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_08_2_NumericCity() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in City Field
		newCustomerPage.inputCity(strNumericCity2);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_09_1_SpecialCity() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In City Field
		newCustomerPage.inputCustomerName(strSpecialCity1);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_09_2_SpecialCity() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In City Field
		newCustomerPage.inputCustomerName(strSpecialCity2);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_10_BlankCity() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputCity(strBlankCity);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_11_StateEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in State Field
		newCustomerPage.inputState(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyStateMsg());
	}

	@Test
	public void Register_12_1_NumericState() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in State Field
		newCustomerPage.inputState(strNumericState1);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_12_2_NumericState() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in State Field
		newCustomerPage.inputState(strNumericState2);

		// verify
		verifyTrue(newCustomerPage.isDisplayNumericMsg());
	}

	@Test
	public void Register_13_1_SpecialState() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In State Field
		newCustomerPage.inputCustomerName(strSpecialState1);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_13_2_SpecialState() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In State Field
		newCustomerPage.inputCustomerName(strSpecialState2);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_14_BlankState() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputState(strBlankState);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_15_1_NumericPin() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter numeric value in Pin Field
		newCustomerPage.inputPin(strNumericPin);

		// verify
		verifyTrue(newCustomerPage.isDisplayIllegalCharacterMsg());
	}

	@Test
	public void Register_16_PinEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in Pin Field
		newCustomerPage.inputPin(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyPinMsg());
	}

	@Test
	public void Register_17_LessPin() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Pin Field
		newCustomerPage.inputPin(strLessPin);

		// verify
		verifyTrue(newCustomerPage.isDisplayPinLessCharacterMsg());
	}

	@Test
	public void Register_18_1_SpecialPin() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Pin Field
		newCustomerPage.inputCustomerName(strSpecialPin1);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_18_2_SpecialPin() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Pin Field
		newCustomerPage.inputCustomerName(strSpecialPin2);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_19_BlankPin() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputPin(strBlankPin);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_21_PhoneEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in PhoneNo Field
		newCustomerPage.inputPhoneNo(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyPhoneMsg());
	}

	@Test
	public void Register_22_PhoneBlank() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter First character Blank space
		newCustomerPage.inputPhoneNo(strBlankPhone1);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@Test
	public void Register_23_PhoneBlank() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Blank space in Telephone
		newCustomerPage.inputPhoneNo(strBlankPhone2);

		// verify
		verifyTrue(newCustomerPage.isDisplayIllegalCharacterMsg());
	}

	@Test
	public void Register_24_1_SpecialPhone() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Telephone Field
		newCustomerPage.inputPhoneNo(strSpecialPhone1);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_24_2_SpecialPhone() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Telephone Field
		newCustomerPage.inputPhoneNo(strSpecialPhone2);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_24_3_SpecialPhone() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Special Character In Telephone Field
		newCustomerPage.inputPhoneNo(strSpecialPhone3);

		// verify
		verifyTrue(newCustomerPage.isDisplaySpecialMsg());
	}

	@Test
	public void Register_25_MailEmpty() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Do not enter a value in Mail Field
		newCustomerPage.inputEmail(Keys.TAB);

		// verify
		verifyTrue(newCustomerPage.isDisplayEmptyMailMsg());
	}

	@Test
	public void Register_26_1_InvalidMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter invalid email in Email field
		newCustomerPage.inputEmail(strInvalidEmail1);

		// verify
		verifyTrue(newCustomerPage.isDisplayInvalidMailMsg());
	}

	@Test
	public void Register_26_2_InvalidMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter invalid email in Email field
		newCustomerPage.inputEmail(strInvalidEmail2);

		// verify
		verifyTrue(newCustomerPage.isDisplayInvalidMailMsg());
	}

	@Test
	public void Register_26_3_InvalidMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter invalid email in Email field
		newCustomerPage.inputEmail(strInvalidEmail3);

		// verify
		verifyTrue(newCustomerPage.isDisplayInvalidMailMsg());
	}

	@Test
	public void Register_26_4_InvalidMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter invalid email in Email field
		newCustomerPage.inputEmail(strInvalidEmail4);

		// verify
		verifyTrue(newCustomerPage.isDisplayInvalidMailMsg());
	}

	@Test
	public void Register_26_5_InvalidMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter invalid email in Email field
		newCustomerPage.inputEmail(strInvalidEmail5);

		// verify
		verifyTrue(newCustomerPage.isDisplayInvalidMailMsg());
	}

	@Test
	public void Register_27_BlankMail() {

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Enter Blank space in Email
		newCustomerPage.inputEmail(strBlankEmail);

		// verify
		verifyTrue(newCustomerPage.isDisplayBlankCharacterMsg());
	}

	@AfterClass
	public void afterClass() {
		closeBrowser();
	}

}
