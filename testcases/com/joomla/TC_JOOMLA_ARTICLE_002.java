package com.joomla;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.joomla.pages.ArticlePage;
import com.joomla.pages.IndexPage;
import com.joomla.pages.LoginPage;
import com.joomla.pages.NewArticlePage;
import com.joomla.pages.PageFactoryPage;

import commons.AbstractTest;

public class TC_JOOMLA_ARTICLE_002 extends AbstractTest {
	WebDriver driver;
	String username, password, oldTitleArticle, newTitleArticle, oldCategory, newCategory, oldContent, newContent;

	private LoginPage loginPage;
	private IndexPage indexPage;
	private ArticlePage articlePage;
	private NewArticlePage newArticlePage;

	@Parameters({ "browser", "url", "version" })
	@BeforeClass
	public void beforeClass(String browser, String url, @Optional("firefox") String version) {
		driver = openMultiBrowser(browser, url, version);

		username = "automationfc";
		password = "automationfc";

		oldTitleArticle = "ngantt article" + randomNumber();
		newTitleArticle = "ngantt article edit" + randomNumber();
		oldCategory = "Blog";
		newCategory = "Uncategorised";
		oldContent = "demo article" + randomNumber();
		newContent = "edit article" + randomNumber();
	}

	@Test
	public void TO_JOOMLA_ARTICLE_001() {
		log.info("go to Login Page");
		loginPage = PageFactoryPage.getLoginPage(driver);

		loginPage.setUserName(username);
		loginPage.setPassword(password);
		indexPage = loginPage.clickLoginBtn();

		log.info("go to New Article Page");
		newArticlePage = indexPage.goToNewAriclePage();
		articlePage = newArticlePage.createNewArticle(oldTitleArticle, oldCategory, oldContent);

		log.info("verify added article");
		verifyTrue(articlePage.isDisplayArticle(oldTitleArticle));

	}

	@Test
	public void TO_JOOMLA_ARTICLE_002() {
		log.info("Edit article");
		articlePage = articlePage.goToAriclePage();
		newArticlePage = articlePage.editArticle(oldTitleArticle);

		articlePage = newArticlePage.createNewArticle(newTitleArticle, newCategory, newContent);

		log.info("verify edited article");
		verifyTrue(articlePage.isDisplayArticle(newTitleArticle));
	}

	@AfterClass
	public void afterClass() {
		closeBrowser();
	}
}
