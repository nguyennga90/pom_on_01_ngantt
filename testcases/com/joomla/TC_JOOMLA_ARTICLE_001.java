package com.joomla;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.joomla.pages.ArticlePage;
import com.joomla.pages.IndexPage;
import com.joomla.pages.LoginPage;
import com.joomla.pages.NewArticlePage;
import com.joomla.pages.PageFactoryPage;

import commons.AbstractTest;

public class TC_JOOMLA_ARTICLE_001 extends AbstractTest{
	WebDriver driver;
	String username, password, strCategory, strContent;
	String titleArticle;
	
	private LoginPage loginPage;
	private IndexPage indexPage;
	private NewArticlePage newArticlePage;
	private ArticlePage articlePage;

	@Parameters({ "browser", "url", "version" })
	@BeforeClass
	public void beforeClass(String browser, String url, @Optional("firefox") String version) {
		driver = openMultiBrowser(browser, url, version);
		
		username = "automationfc";
		password = "automationfc";
		
		titleArticle = "ngantt article" + randomNumber();
		strCategory = "Blog";
		strContent = "demo article" + randomNumber();
	}

	@Test
	public void TO_JOOMLA_ARTICLE_001() {
		log.info("go to Login Page");
		loginPage = PageFactoryPage.getLoginPage(driver);
		
		loginPage.setUserName(username);
		loginPage.setPassword(password);
		indexPage = loginPage.clickLoginBtn();
		
		log.info("go to New Article Page");
		newArticlePage = indexPage.goToNewAriclePage();
		articlePage = newArticlePage.createNewArticle(titleArticle, strCategory, strContent);
		
		log.info("verify added article");
		verifyTrue(articlePage.isDisplayArticle(titleArticle));
	}

	@AfterClass
	public void afterClass() {
		closeBrowser();
	}
}
