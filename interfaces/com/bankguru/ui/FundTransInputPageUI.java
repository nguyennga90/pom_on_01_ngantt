package com.bankguru.ui;

public class FundTransInputPageUI {

	public static final String PAYER_ACC_NO_TXT = "//input[@name='payersaccount']";
	public static final String PAYEE_ACC_NO_TXT = "//input[@name='payeeaccount']";
	public static final String AMOUNT_TXT = "//input[@name='ammount']";
	public static final String DESCRIPTION_TXT = "//input[@name='desc']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String RESET_BTN = "//input[@name='res']";
}
