package com.bankguru.ui;

public class DepositPageUI {

	public static final String ACCOUNT_NO_TXT = "//input[@name='accountno']";
	public static final String AMOUNT_TXT = "//input[@name='ammount']";
	public static final String DES_TXT = "//input[@name='desc']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String RESET_BTN = "//input[@name='res']";
}
