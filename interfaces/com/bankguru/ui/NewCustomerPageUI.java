package com.bankguru.ui;

public class NewCustomerPageUI {

	public static final String CUSTOMER_NAME_TXT = "//input[@name='name']";
	public static final String GENDER_MALE_RDO = "//input[@value='m']";
	public static final String GENDER_FEMALE_RDO = "//input[@value='f']";
	public static final String DATE_OF_BIRTH = "//input[@id='dob']";
	public static final String ADDRESS_TXA = "//textarea[@name='addr']";
	public static final String CITY_TXT = "//input[@name='city']";
	public static final String STATE_TXT = "//input[@name='state']";
	public static final String PIN_TXT = "//input[@name='pinno']";
	public static final String PHONE_NO_TXT = "//input[@name='telephoneno']";
	public static final String EMAIL_TXT = "//input[@name='emailid']";
	public static final String PASSWORD_TXT = "//input[@name='password']";
	public static final String SUBMIT_BTN = "//input[@name='sub']";
	public static final String RESET_BTN = "//input[@name='res']";
	
	public static final String CUSTOMER_NAME_EMPTY_TEXT = "//label[contains(text(),'Customer name must not be blank')]";
	public static final String CUSTOMER_NAME_NUMERIC_TEXT = "//label[contains(text(),'Numbers are not allowed')]";
	public static final String CUSTOMER_NAME_SPECIAL_TEXT = "//label[contains(text(),'Special characters are not allowed')]";
	public static final String CUSTOMER_NAME_CHARACTER_TEXT = "//label[contains(text(),'First character can not have space')]";
	public static final String ADDRESS_EMPTY_TEXT = "//label[contains(text(),'Address Field must not be blank')]";
	public static final String CITY_EMPTY_TEXT = "//label[contains(text(),'City Field must not be blank')]";
	public static final String STATE_EMPTY_TEXT = "//label[contains(text(),'State must not be blank')]";
	public static final String PIN_EMPTY_TEXT = "//label[contains(text(),'PIN Code must not be blank')]";
	public static final String PIN_LESS_CHARACTER_TEXT = "//label[contains(text(),'PIN Code must have 6 Digits')]";
	public static final String PHONE_EMPTY_TEXT = "//label[contains(text(),'Mobile no must not be blank')]";
	public static final String MAIL_EMPTY_TEXT = "//label[contains(text(),'Email-ID must not be blank')]";
	public static final String INVALID_EMAIL_TEXT = "//label[contains(text(),'Email-ID is not valid')]";
	public static final String ILLEGAL_CHARACTER_TEXT = "//label[contains(text(),'Characters are not allowed')]";
}
