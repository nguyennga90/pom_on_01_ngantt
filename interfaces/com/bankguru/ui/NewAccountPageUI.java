package com.bankguru.ui;

public class NewAccountPageUI {

	public static final String CUSTOMERID_TXT = "//input[@name='cusid']";
	public static final String ACCOUNT_TYPE_PLD = "//select[@name='selaccount']";
	public static final String INITIAL_DEPOSIT_TXT = "//input[@name='inideposit']";
	public static final String SUBMIT_BTN = "//input[@name='button2']";
	public static final String RESET_BTN = "//input[@name='reset']";
}
