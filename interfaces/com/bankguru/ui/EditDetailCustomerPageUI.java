package com.bankguru.ui;

public class EditDetailCustomerPageUI {

	public static final String ADDRESS_TXA = "//textarea[@name='addr']";
	public static final String CITY_TXT = "//input[@name='city']";
	public static final String STATE_TXT = "//input[@name='state']";
	public static final String PIN_TXT = "//input[@name='pinno']";
	public static final String PHONE_NO_TXT = "//input[@name='telephoneno']";
	public static final String MAIL_TXT = "//input[@name='emailid']";
	public static final String SUBMIT_BTN = "//input[@name='sub']";
	public static final String RESET_BTN = "//input[@name='res']";
}
