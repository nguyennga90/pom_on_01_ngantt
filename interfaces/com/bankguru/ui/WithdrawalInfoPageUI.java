package com.bankguru.ui;

public class WithdrawalInfoPageUI {

	public static final String SUCCESS_TEXT = "//p[contains(text(),'Transaction details of Withdrawal for Account %s')]";
	public static final String INFO_TEXT = "//table[@id='withdraw']//td[contains(text(), '%s')]//following-sibling::td";
	
}
