package com.bankguru.ui;

public class DepositInfoPageUI {

	public static final String SUCCESS_TEXT = "//p[contains(text(),'Transaction details of Deposit for Account %s')]";
	public static final String INFO_TEXT = "//table[@id='deposit']//td[contains(text(), '%s')]//following-sibling::td";
	
}
