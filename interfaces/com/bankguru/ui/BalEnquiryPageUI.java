package com.bankguru.ui;

public class BalEnquiryPageUI {

	public static final String SUCCESS_TEXT = "//p[contains(text(),'Balance Details for Account %s')]";
	public static final String INFO_TEXT = "//table[@id='balenquiry']//td[contains(text(), '%s')]//following-sibling::td";
	
}
