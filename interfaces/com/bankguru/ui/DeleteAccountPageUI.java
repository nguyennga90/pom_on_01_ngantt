package com.bankguru.ui;

public class DeleteAccountPageUI {

	public static final String ACCOUNT_NO_TXT = "//input[@name='accountno']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String RESET_BTN = "//input[@name='res']";
	
	public static final String MSG_SUCCESS = "Account Deleted Sucessfully";
}
