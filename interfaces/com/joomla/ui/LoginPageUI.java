package com.joomla.ui;

public class LoginPageUI {

	public static final String USER_TXT = "//input[@name='username']";
	public static final String PASS_TXT = "//input[@name='passwd']";
	public static final String LOGIN_BTN = "//button[contains(@class,'login-button')]";
}
