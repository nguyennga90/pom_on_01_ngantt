package com.joomla.ui;

public class IndexPageUI {

	public static final String CONTENT_MENU = "//ul[@id='menu']//a[contains(text(),'Content')]";
	public static final String ARTICLE_MENU = "//ul[@id='menu']//a[contains(@class,'menu-article')]";
	public static final String NEW_ARTICLE_MENU = "//ul[@id='nav-empty']//a[contains(text(),'Add New Article')]";
	
}
