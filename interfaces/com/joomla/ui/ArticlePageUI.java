package com.joomla.ui;

public class ArticlePageUI {

	public static final String ARTICLE_TITLE_LNK = "//table[@id='articleList']//a[contains(text(),'%s')]";
	public static final String ARTICLE_CBX = "//a[contains(text(),'%s')]/ancestor::tr//input[contains(@id,'cb')]";
	public static final String EDIT_BTN = "//div[@id='toolbar-edit']/button";
}
