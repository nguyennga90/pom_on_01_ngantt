package com.joomla.ui;

public class NewArticlePageUI {

	public static final String TITLE_TXT = "//input[@id='jform_title']";
	public static final String CATEGORY_CBX = "//div[@id='jform_catid_chzn']";
	public static final String CATEGORY_ITEM = "//div[@id='jform_catid_chzn']//li[contains(text(),'%s')]";
	public static final String ARTICLE_IFRAME = "//iframe[@id='jform_articletext_ifr']";
	public static final String SAVE_CLOSE_BTN = "//div[@id='toolbar-save']//button";
	
}
